name='outlier.ppm';
name2='outlier2.png';
im=imread(name);
[H,W,~]=size(im);

im2=zeros(H,W,3);
for h=1:H
    for w=1:W
        im2(h,w,:)=255;
    end
end
radius=3;
cnt=0;
for h=1:H
    for w=1:W
        if ~(im(h,w,1)==0 && im(h,w,2)==0 && im(h,w,3)==0)
            cnt=cnt+1;
           
            for hh=h-radius:h+radius
                for ww=w-radius:w+radius
                    if hh<1 || hh>H || ww<1 || ww>W
                        continue;
                    end
                    dist=sqrt((hh-h)*(hh-h)+(ww-w)*(ww-w));
                    if dist<3
                        im2(hh,ww,:)=im(h,w,:);
                    end
                end
            end
           
        end
    end
end

imwrite(im2,name2);