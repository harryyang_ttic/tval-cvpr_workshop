\documentclass[10pt,twocolumn,letterpaper]{article}
\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{array}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
%\usepackage{hyperref}


\DeclareMathOperator*{\argmin}{arg\,min}

\newcommand{\ignore}[1]{}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}

% \cvprfinalcopy % *** Uncomment this line for the final submission
\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}


% MRJ notes: use ``registration'' instead of ``calibration'' for inter-sensor transformations?


%%%%%%%%% TITLE
\title{Sensor-Prediction Evaluation of Stereo, Optical Flow and Scene Flow Algorithms}

\author{First Author\\
Institution1\\
Institution1 address\\
{\tt\small firstauthor@i1.org}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Second Author\\
Institution2\\
First line of institution2 address\\
{\tt\small secondauthor@i2.org}
}

\maketitle
%\thispagestyle{empty}
\begin{abstract}
We present a novel dataset and a benchmark suite Tval, to address the problem of quantitative evaluation of vision systems on images in the wild.  
The dataset is recorded using a test vehicle with a velodyne laser scanner and a pair of high resolution cameras. Motivated by the difficulty of obtaining the ground truth for stereo, optical flow and scene flow, we present a method for quantitative evaluation of pixel-level vision algorithms based directly on lidar data and without ground truth labeling. Our evaluation methodology allows for evaluation on complex dynamic in-the-wild scenes. A key component of our benchmark suite is that it provides converters which can be used to predict lidar distance measureents (flight times) based on stereo and flow result. Our evaluation method opens the practical possibility of autonomous learning of vision algorithms --- robotic learning from simply being in the world. We have scored state-of-art vision algorithms, and the preliminary version of our benchmarks is available online at \href{http://tval.ttic.edu}{tval.ttic.edu}.
\end{abstract}

\section{Introduction}

We are motivated by the need for in-the-wild quantitative evaluation of pixel-level vision
algorithms such as stereo, optical flow and scene flow. In-the-wild evaluation is non-trivial
because ground truth is difficult to obtain and susceptible to errors. Optical flow is typically
evaluated on synthetic data such as the Yosemite sequence \cite{austvoll2005study} and more recently
the MPI Sintel dataset \cite{butler2012naturalistic}. Difficulties in obtaining ground truth has had
significant consequences for the KITTI stereo data set \cite{geiger2012we,Geiger2013IJRR}, as it
requires manual processing and also leads to imperfect labeling
(Fig.~\ref{fig:kitti_error}). More specifically, in order to improve density the KITTI
ground truth is constructed from data collected from several different frames of lidar
images.  To create the ground truth disparity maps and optical flow maps the lidar points from different times
must be placed in the corrdinate system of the image data. For the KITTI data set this is done by estimating the ego-motion of the car and
using the assumption that the scene points are not moving.  As a result, the KITTI optical flow data
set contains only static scenes --- scenes without other moving cars or moving pedestrians.  On the
other hand, Sintel uses animated scenes which are significantly different from realistic scenes. Here we
describe a meaningful quantitative evaluation for in-the-wild dynamic scenes.

\begin{figure}
\centering
\includegraphics[width=1\columnwidth]{img/kitti/outlier4.pdf}
\caption{A visualization of KITTI imperfection. A disparity threshold is used to color lidar points on the background blue and forground lidar points on the car red. The ground truth wrongly labels some portion of the background as car.}
\label{fig:kitti_error}
\end{figure}

The Tval evaluation avoids inferring ground truth by instead considering the task of predicting
lidar data from image data. We use lidar distance measurement prediction as the evaluation metric.  Each
lidar point is obtained from a laser pulse sent at a certain time from an certain position (origin)
in a certain direction.  The task is to take the image data and a set of lidar pulse specifications (pulse time, origin, and
direction) and to predict the measured distance (flight time) for each pulse specification.  For stereo the
image data consists of a single stereo pair and the task is to predict the distance measurement for lidar
pulses sent at (nearly) the time at which the stereo image was taken.  For optical flow the image data
consists of two stereo image pairs taken 100 milliseconds apart in time.  The lidar pulses were sent
approximately 100 millisecond {\em after} the second stereo image was taken.

Tval provides training data as well as an evaluation server. The training data consists of image
data and lidar data. For both stereo and optical flow the test data is similar to the training data except that in the train
data the measured distance of each pulse is provided. Currently our stereo dataset contains 873 stereo pairs, each has a left-right pair of
monochrome images and a lidar file (Fig.~\ref{fig:intro}). Our optical flow/scene flow dataset
contains 195 video sequences, each has two image pairs at time $t_1$ and $t_2$, and a third left
image at time $t_3$ with the corresponding lidar file.  {\color{red} Earlier we say that the measured distance is given rather than a
a 3D point. We need to be careful here to describe the actual files.  I think it would be better if the files contained distance (in meters) rather than 3D points.
Actually the conversion of distance (flight time) to a 3D point involves a calibration.  To really get the distance measurement (flight time)
we probably need an ealier more ``raw'' stage of the data.}

\begin{figure} %\left
\includegraphics[width=0.5\textwidth]{img/fig1/image1_top2.pdf}
\caption{Visualization of a stereo pair and corresponding lidar file. \textbf{Top:} the stereo pair. Images are monochrome and have resolution of 1600 x 848. The zoom in shows the viewpoint difference between left and right camera. \textbf{Bottom:} visualization of the lidar file. The lidar took a full panoramic scan of 145, 927 lidar points. Zoom in shows the lidar points of the moving cars.}
\label{fig:intro}
\end{figure}

To facilitate the evaluation of traditional disparity and optical flow maps the Tval evaluation
suite provides ``converters''.  A stereo converter converts a disparity map to lidar predictions and
an optical flow converter converts an optical flow map to lidar predictions.  While the converts allow for the evaluation
of traditional disparity and optical flow maps, the converters inevitably
incorporate biases.  A user can avoid the biases of the Tval converters by writing their own converters.
Ultimately the evaluation task in Tval is to predict the lidar distance measurements (flight times) from the image data.
The Tval converters do not make use of the test-tme distance measurements so that in all cases the distance predictions are
constructed entirely from the image data.

Although the Tval converters do not make use of test-time distance measurements, the converters have parameters
where the parameter values have been learned on the training data.  Most significantly, the converters rely on parameters specifying
the relative geometry of the camera and the lidar system and calibration parameters for the lidar unit itself.
These calibration parameters are also provided and a user can
incorporate Tval's calibration parameters into their own converter or do their own calibration based on the training data.
{\color{red} We need to say more precisely what the lidar pulse specification in the file consist of.  Are we giving pre-calibration
information about the lidar pulses?  Or are the data files constructed from TRINA's group calibration of the velodyne?}
When using the Tavl converters these details are invisible as the converters take as input traditional disparity maps or optical flows.

An important aspect of Tval's ``sensor prediction'' approach to evaluation is the potential for autonomous learning.
By ``autonomous learning'' we mean learning
from interaction with the world without the use of externally provided training data.
Machine learning involves tuning a model so as to optimize performance.  The point here is that
performance can be defined entirely in terms of lidar flight time predictions.  Hence {\em perceptual systems can be tained without the use of independent labeling}. Of particular interest is the possibility of training of neural networks directly from lidar distance measurements.  We note that the current leading system on KITTI leader board uses a DNN disparity match energy~\cite{vzbontar2014computing}.

\section{Scoring}

We use lidar predictions as our ultimate evaluation.
We consider a lidar pulse with measured distance $r$ and predicted distance $\hat{r}$.
We define the ``pixel error'' of this prediction to be
\begin{equation}\label{eq:threshold}
\mathbf{err}(\hat{r},r) = \frac{|\hat{r}-r|fb}{r\hat{r}}
\end{equation}
where $f$ is the focal length of the cameras in pixels and b is the baseline separation between the stereo cameras.
Note that $\mathbf{err}(\hat{r},r)$ has units of pixels --- the distance unit cancels out.
This error measure roughly corresponds to pixel error in the disparity map for the stereo problem.  To see this we note the following
where $Z$ is roughly the $Z$ coordinate, in camera coordinates, of the point in space that was hit by the lidar pulse and $d$ and $\hat{d}$ are the true
and esitmated disparities in pixels.
\begin{eqnarray*}
Z & \approx & r \\
\\
\hat{Z} & \approx & \hat{r} \\
\\
d & = & \frac{fb}{Z} \; \approx \; \frac{2fb}{r} \\
\\
\hat{d} & = & \frac{fb}{\hat{Z}} \;\approx \; \frac{2fb}{\hat{r}}
\end{eqnarray*}
We then get $\mathbf{err}(\hat{r},r) \approx |\hat{d} - d|$.
The $k$-pixel error rate is then the fraction of lidar pulse predictions for which $\mathbf{err}(\hat{t},t) > k$.

It is important to note that the score is defined independent of any calibration parameters.
This ensures that there is no chance for the results to be biased by a choice of calibration method.

\noindent\textbf{Outlier lidar pulses.}
The Tval score is computed from a ``inlier subset'' of the lidar pulses actually sent by the lidar system. Specifically, for each lidar point $p_i$, we compute the mean distance $d_i=\frac{1}{k}\sum\limits_{j=1}^kd_{ij}$ from $p_i$ to its $k$-nearest neighbors, where $d_{ij}=|r_i-r_j|$ and $r_i, r_j$ are the flight time of pulse $i,j$ divided by the speed of light $c$. We assume the resulted distribution of $n$ total points to be Gaussian, whose mean is $\mu=\frac{1}{n}\sum\limits_{i=1}^n d_i$ and the standard deviation is $\sigma$. The outlier subset is defined to be the points $\{p_i\}$ whose mean distance $d_i$ is outside the 95\% interval defined as $d_{\pm}=\mu\pm 1.96\cdot\sigma$ and are trimmed from the dataset. The classification of non-scored outlier points is defined independent of camera lidar geometry to avoid bias. 
{\color{red} After thinking about this a bit more, I am confused by the description.  What is $d_{i,j}$.  I was thinking it was $|r_i - r_j|$ where $r_i$ is the flight time of pulse $i$
divided by the speed of light $c$.  But the description does not make this clear.  Also, I would have thaought that $|r_i - \mu_i|$ would be better where
$\mu_i = (1/k)\sum r_k$.}

\noindent\textbf{Occluded lidar pulses.}
Tval reports the error rate on all lidar pulses, ``non-occluded'' pulses, and ``occluded'' pulses. In order to effectively detect occluded pulses, we represent each point as lidar voxel and build an octree to facilitate pulse tracing. Given a specific camera-lidar geometry, a lidar pulse identified as occluded if the path from the camera to the lidar point is blocked by a voxel. {\color{red} I still think that we should classify the points into easy and hard using the simple formulas that I had on my board the other day.}

\section{The Stereo Converter}
%\label{sec:calibrate}
%Converters are provided as a convenience gateway to submit conventional stereo, optical flow and
%scene flow results, which after conversion are used to predict lidar flight time given the lidar
%pulse directions. This process involves the estimate of the lidar-camera geometry (a geometry
%calibration), since the vision results are usually in camera coordinate and the lidar pulses are in
%lidar coordiante. Tval differs from other evaluation systems in that it allows using a calibrated
%lidar-camera geometry in converters, as calibration is taken as part of the prediction process here
%rather than the evaluation process. A method to calibrate lidar-camera geometry is described in
%Section~\ref{sec:calibrate} and the parameters are accessible as default parameters. In addition,
%teams are free to calibrate on their own and enter lidar predictions directly independent of the
%calibration used in Tval's converters.

%\noindent\textbf{The stereo converter.}
We provide two types of converters for stereo. One is the slanted plane converter, which first performs slanted-plane smoothing of the given disparity map using the method of \cite{yamaguchi2014efficient}. Slanted plane smoothing results in a system of slanted disparity planes $\{\theta_i\}$, where each plane $\theta_i=(A_i,B_i,C_i)$ defines a segment $i$ in image and a disparity estimate at each pixel within the segment by $d(p,\theta_i)=A_ip_x+B_ip_y+C_i$. 

Given $\{\theta_i\}$ and a particular estimate of lidar-camera geometry, the flight time of a lidar pulse is predicted by linearly tracing a ray in the image corresponding the position of lidar pulse as a function of time. Specifically, given the lidar origin position $P_o$ in camera frame and the directional vector $\vec{v}$ of lidar pulse, the position of the lidar pulse can be expressed as $P_t=P_o+ct\vec{v}$ and the corresponding position on image plane is given by $p_t=M_{proj}P_t$ where $M_{proj}$ is the camera projection matrix. The disparity at $p_t$ is given by $d(p_t,\theta_j)$ if $p_t$ is in segment $j$. The ray tracing terminates when $d(p_t,\theta_j)$ indicates that $P_t$ is behind the slanted plane surface seen in the image. The lidar pulse is then assigned to one of the segments of the image --- either the visible segment $j$ ``hit'' by the pulse \textit{or} the segment that the pulse passed over before being ``occluded'' by a neighboring segment based on whether the pulse-plane intersection is inside or outside the segment $j$ (Fig.~\ref{fig:occlusion}). Once the pulse has been assigned to a segment, the flight time $\hat{t}$ can be predicted by solving for the point in space where the pulse intersects the plane defined by the associated segment.

\begin{figure}
\centering
\begin{subfigure}[b]{0.45\textwidth}
 				\includegraphics[width=\textwidth]{img/img4/occlusion7.pdf}
                \label{fig:occlude}
\end{subfigure}
\caption{The lidar pulse tracing model. \textbf{Left:} the lidar pulse terminates at point $P$ where it hits the slanted disparity plane $p_2$ occluded by $p_1$. The pulse is then assigned to plane $p_2$. \textbf{Right:} the corresponding trajectory of the lidar ray on image, where the ray hits the car in the back (invisible to camera). Each segment on image represents a slanted disparity plane.}
\label{fig:occlusion}
\end{figure}

Another type of stereo converter is the raw disparity converter. Instead of performing slanted-plane smoothing of the given disparity map, it takes the value of raw disparity at each pixel and uses it as the depth estimation. Similarly we trace the ray until its location is behind the depth of the corresponding pixel. In our experiment, slanted plane converter generally performs better than the raw disparity converter, mostly because the smoothing results in better robustness to noise and the plane model incorporates the geometry information of the scene.

\noindent\textbf{The optical flow converter.} 
The optical flow evaluation is based on the algorithm's ability of predicting the lidar flight time at time $t_3$ given the image pairs at time $t_1$ and $t_2$. The converter first tries to predict the lidar point cloud at time $t_3$ based on the depth estimation and flow information. Specifically, it takes the depth of image pixels taken at time $t_1$ and $t_2$ (for optical flow evaluation, the depth can be acquired using off-the-shelf stereo algorithms and is part of the converter) and their correspondences given the flow, and estimates the motion of the points in space. We then start at time $t_2$ and apply the motion and get the points at time $t_3$. Next we use either the slanted plane converter or the raw disparity converter to predict the lidar flight time. In addition, this converter can be easily extended to scene flow evaluation which estimates the 3D depth and motion simultaneously.

Converters source code including default calibration are publicly available as a development kit. This will allow for development-time evaluation on Train-Val splits of the training data. 

\noindent\textbf{Calibration of Camera-Lidar Geometry.}
As part of the converter, we describe our method to calibrate the geometry between camera and lidar using the training data. The calibration is based on the result of~\cite{yamaguchi2014efficient} and the the slanted plane stereo converter. Starting from a initial set of camera-lidar geometry configuration, we iteratively update the transform between camera and lidar to minimize the $k$-pixel error rate of the set of disparity planes. The assignment of lidar pulse to image segment also changes with camera-lidar geometry. The calibrated geometry is the transform that best aligns the slanted disparity planes with the actual lidar positions.

\section{Data Collection}

\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{img/trina_car_plain.pdf}
\caption{Test vehicle used for data collection.  The Velodyne HDL-64E Lidar and stereo camera setup are mounted on the roof rack.  Details of the sensors are in the text.}
\label{fig:test_vehicle}
\end{figure}

The test vehicle has a similar hardware setup to many other autonomous
vehicle projects \cite{Levinson-RSS-07, urmson2008autonomous, Geiger2013IJRR}.
The pose and motion estimates come from an Applanix POS-LV 220 inertial GPS 
navigation system.  This system generates pose estimates at 100 Hz.  The Lidar data
is from a Velodyne HDL-64E, which uses 64 laser beams at fixed vertical angles, and spins at 10
Hz.  Each measured point is computed using the vertical angle, horizontal angle, range, and an intensity
value.  The 3D position is computed using the vertical angle, horizontal angle, and range.  The vertical angle
is a fixed measured value, the horizontal angle is measured using a 36,000 tick encoder, and the range
is measured using a resolution of 5cm.  In addition, the Velodyne company has developed custom
calibration and position estimation algorithms for estimating 3D point position, see \cite{Velodyne_manual}
for more details.
The intensity is an 8 bit value which is a measurement of the reflectivity of the surface being illuminated.  

The camera data is from a pair of Prosilica GE1660 Monochrome 
cameras, each with resolution $1600$ x $1200$.  The camera calibration for both intrinsic and extrinsic transformation 
between cameras is performed using the technique of 

Using these calibration parameters, we perform stereo rectification and
then trim top portion of the image.  We have designed a custom camera trigger device that monitors UDP packets 
from the velodyne and synchronizes camera firing when the cameras and Velodyne are aligned.  This elimates inconsistencies 
due to time synchronization effects.  We use a hand-measured transformation matrix between one of the cameras and the 
Velodyne sensor as a starting point for our automatic calibration method.

To generate datasets of relatively independent images, we use the output of our inertial GPS system and record laser / image
data when the vehicle has traveled more than 50 meters from the previous location, or has a yaw difference of more than $85$ degrees.  The consecutive sequences dataset is constructed using the same method.

\section{Results}

We evaluate the state-of-art stereo and optical flow methods on our dataset. We use the 3-pixel error rate result and compare it with KITTI, as we have similar outdoor scenes (Table~\ref{tb:error}).  

\begin{table}[h!]
\begin{center}
\resizebox{0.45\textwidth}{!}{%
  \begin{tabular}{ r | c | c | c | c  }
    \hline
    Stereo & Noc & All & KITTI Noc & KITTI All \\ \hline
    SPS-St~\cite{yamaguchi2014efficient} & 4.55\% & 5.79\% & 3.39\%  &  4.41\%  \\ 
    rSGM~\cite{spangenberg2014large} &5.84\%& 7.33\% & 5.03\% & 6.60\%\\
    opencv-BM~\cite{bradski2000opencv} &6.61\%& 7.88\% & 12.28\% & 13.76\% \\
    opencv-SGBM~\cite{hirschmuller2008stereo} &9.54\%& 10.32\% & 7.64\% &  9.13\%\\
    ELAS~\cite{geiger2011efficient} &10.80\%& 11.09\% &8.24\% & 9.96\%\\ 
    GCS~\cite{cech2007efficient} &23.01\%& 19.91\% & 13.38\% & 14.54\%\\
     S+GF~\cite{zhang2014cross} &30.09\%&34.82\% &11.21\%	& 11.21\% \\
     \hline
  \end{tabular}}
\end{center} 
\caption{3-pixel error rate of several stereo algorithms using slanted-plane stereo converter. We show the error rate for both non-occuluded points and the entire points.}
\label{tb:error}
\end{table}

In general, algorithms like SPS-ST, rSGM, opencv-SGBM and ELAS have similar 3-pixel error rate with KITTI. Note that our dataset is more challenging because KITTI manually removes ambiguous regions while we tend to avoid such processing and include all lidar pulses instead. For other algorithm such as opencv-BM, we see a decrease in error. This indicates that its original block matching algorithm produces noisy result and the slanted-plane converter is giving it performance gain. GCS and S+GF perform worse in our dataset, partially because they are error-prone to ambiguous and saturated regions frequently appearing in our dataset. Fig.~\ref{fig:best_worst} shows examples of images with most and least errors. The ``easy cases'' are those in wide open scenes, while ``hard cases'' are those have objects near to the camera.  
\begin{figure}
\centering
\begin{subfigure}[b]{0.45\textwidth}
 				\includegraphics[width=\textwidth]{img/img6/000168_raytrace.png}
                \label{fig:bestworst}
                \subcaption{3-pixel error: 26.57\%}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
 				\includegraphics[width=\textwidth]{img/img6/000527_raytrace.png}
                \label{fig:bestworst}
                 \subcaption{3-pixel error: 1.33\%}
\end{subfigure}
\caption{The best and worst performed images of SPS-ST. Images are converted to 3D planes and visualized with lidar points mapped on top. The color of lidar points shows whether the algorithm correctly predicts the flight time of the lidar pulse, where red means ``correct predict'' and blue means ``wrong predict''.}
\label{fig:best_worst}
\end{figure}

More interesting cases are the flow result, where contrary to KITTI, Classic++ performs better than SPS-FL. This is because Classic++ can successfully handle moving objects contained in our dataset (Fig.~\ref{tb:flow_error}). 

\begin{figure}
\centering
\begin{subfigure}[b]{0.35\textwidth}
 				\includegraphics[width=\textwidth]{img/flow/c3.pdf}
                \subcaption{Classic++ 3-pixel error: 9.13\%}
\end{subfigure}
\begin{subfigure}[b]{0.35\textwidth}
 				\includegraphics[width=\textwidth]{img/flow/y3.pdf}
                 \subcaption{SPS-FL 3-pixel error: 25.07\%}
\end{subfigure}
 \\[\floatsep]
%\begin{center}
\resizebox{0.45\textwidth}{!}{%
  \begin{tabular}{ r | c | c | c | c  }
    \hline
    Flow & Noc & All & KITTI-Noc & KITTI-All \\ \hline
 	Classic++ ~\cite{sun2014quantitative} & 4.46\% & 8.66\% & 10.49\% & 20.64\% \\
    SPS-Fl~\cite{yamaguchi2014efficient} & 8.17\% & 14.68\% & 3.38\% & 10.06\%\\ 
    \hline
  \end{tabular}}
%\end{center} 
\caption{Above: a test case of optical flow with a car moving forward relative to the camera. Wrong predictions are visualized in color green and blue: green points are the wrong predictions where the actual lidar positions are further, while blue points are where the actual lidar positions are closer. We can see Classic++ successfully captures the forward motion of the car where SPS-FL fails. Below: the 3-pixel error rate of Classic++ and SPS-FL for the entire flow dataset. The depth for the optical flow converter is based on the result of SPS-ST.}
\label{tb:flow_error}
\end{figure}

\section{Conclusion and Future Work}

We presented the benchmark suite Tval and a quantitative approach to answer the question ``how do we understand and evaluate our understanding of the scene without ground truth inference''. Similar to human who can confirm vision with touching, an autonomous car can confirm vision with lidar. The proposed framework combines vision algorithms with different converters, and could be quantitatively evaluated using lidar pulses directly. This indicates we can develop autonomous learning by simply being in the wild. With our evaluation server set up online, we believe the evaluation metric will attract submissions of various vision algorithms. 
%%%%%%%%% BODY TEXT


{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\end{document}
